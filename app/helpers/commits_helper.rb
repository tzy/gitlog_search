module CommitsHelper
  def comment_line(line)
    return line unless line =~ /CR\s(|#|number\s)\d{5}/
    link_text = line.match(/CR\s(|#|number\s)\d{5}/).to_s
    url = 'http://localhost/bugs/'
    if link_text.include?('#') 
      url << link_text.split('#').last
    else
      url << link_text.split(' ').last
    end
    
    line.gsub(link_text, link_to(link_text, url)).html_safe
  end

  def commit_class(commit)
    commit.merge.nil? ? 'commit' : 'commit merge-commit'
  end

  def name_list(commits) 
    commits.collect {|c| [c.name, c.name]}.uniq
  end
end
