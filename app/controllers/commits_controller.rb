class CommitsController < ApplicationController
  def index 
    @commits = Commit.all
    filtering_params(params).each do |key, value|
      @commits = @commits.public_send(key, value) if value.present?
    end
  end

  private 
    
    # A list of the param names that can be used for filtering the Commit list
    def filtering_params(params)
      params.slice(:ref, :author, :date)
    end
end
