class Commit < ActiveRecord::Base
  scope :ref, -> (ref) { where("ref like ?", "%#{ref}%")}
  scope :author, -> (author) { where("name =?", author)}
  scope :date, -> (date) { where("DATE(date) =?", Date.parse(date))}
  scope :order_by_name, -> { order(:name) }
end
