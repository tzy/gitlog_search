if (history && history.pushState) {
  $(document).ready(function(){
    $('.search #date').datepicker({
      format: "yyyy-mm-dd"
    }); 

    $('#commits_search').submit(function() {
      $.get(this.action, $(this).serialize(), null, 'script');
      history.pushState(null, '', this.action + '?' + $(this).serialize()); 
      return false;
    });

    $(window).bind("popstate", function() {
      $.getScript(location.href);
    });
  });
}