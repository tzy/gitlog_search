class CreateCommits < ActiveRecord::Migration
  def change
    create_table :commits do |t|
      t.string :ref
      t.string :merge
      t.string :author
      t.string :name
      t.string :email
      t.datetime :date
      t.string :comments

      t.timestamps null: false
    end
  end
end
