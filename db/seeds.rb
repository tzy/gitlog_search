logs = File.read('git.log')
logs = logs.split('commit ')
logs.shift
 
logs = logs.map do |log|
  item = Hash.new 
  comments = Array.new
  l = log.split("\n")
  item[:ref] = l.shift
  l.each do |i|
    if i.include? 'Merge: '
      item[:merge] = i.to_s.split('Merge: ')[1].to_s.strip  
    elsif i.include? 'Author: '
      author = i.to_s.split('Author: ')[1] 
      x = author.to_s.split(' ')
      item[:author] = author
      item[:email] = x.pop
      item[:name] = x.join(' ')
    elsif i.include? 'Date: '
      item[:date] = i.to_s.split('Date: ')[1].to_s.strip  
    else
      comments << i.to_s
    end
  end
  item[:comments] = comments.join('\n')
  item

end

Commit.create(logs)
