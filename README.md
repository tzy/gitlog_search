#GitWeb

###1. Clone this repository(or unzip the source file) into a directory of your choice.
> git clone https://tzy@bitbucket.org/tzy/gitlog_search.git

###2. Go to the directory and install
> $ cd gitlog_search 

> $ bundle install

###3. Create database and seed data
> rake db:create db:migrate db:seed

###4. Running locally 
> $ rail s

you access the gitweb page at http://localhost:3000

###5. To run rspec tests
From inside the 'gitlog_search' directory, run:
> $ rspec

the terminal output should be:
> .................

> Finished in 0.2653 seconds (files took 4.89 seconds to load)

> 17 examples, 0 failures
