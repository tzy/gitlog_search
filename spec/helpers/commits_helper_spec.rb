require 'rails_helper'
require 'helpers'

describe CommitsHelper do 
  include Helpers

  context '#comment_line' do

    let(:displayed_comments_with_hyperlink1) { 
      "<a href=\"http://localhost/bugs/38743\">CR 38743</a>. Fixing old rollup helpers to work with new models." 
    }
    let(:displayed_comments_with_hyperlink2) { 
      "<a href=\"http://localhost/bugs/39874\">CR #39874</a>" 
    }
    let(:displayed_comments_with_hyperlink3) { 
      "<a href=\"http://localhost/bugs/38161\">CR number 38161</a>. Fixing regular expression for matching IPv4 addresses." 
    }

    it 'returns comment' do 
      comment = 'Fixing old rollup helpers to work with new models.'
      expect(comment_line(comment)).to eq(comment)
    end 

    it 'returns comment with hyperlink (bug referencing pattern: CR 38743)' do 
      comment = 'CR 38743. Fixing old rollup helpers to work with new models.'
      expect(comment_line(comment)).to eq(displayed_comments_with_hyperlink1)
    end 
    
    it 'returns comment with hyperlink (bug referencing pattern: CR #39874)' do
      comment = 'CR #39874'
      expect(comment_line(comment)).to eq(displayed_comments_with_hyperlink2)
    end

    it 'returns comment with hyperlink (bug referencing pattern: CR number 38161)' do
      comment = 'CR number 38161. Fixing regular expression for matching IPv4 addresses.'
      expect(comment_line(comment)).to eq(displayed_comments_with_hyperlink3)
    end
  end 

  context '#commit_class' do
    let(:commit) { double('commit', :merge => nil) }
    let(:merge_commit) { double('merge_commit', :merge => 'ae5a30e 76bde21') }

    it 'returns class name for commit' do 
      expect(commit_class(commit)).to eq('commit')
    end 

    it 'returns class name for merge commit' do 
      expect(commit_class(merge_commit)).to eq('commit merge-commit')
    end 

  end

  context '#name_list' do  
    before do
        Commit.delete_all
        Commit.create!([fake_commit1_hash, fake_commit2_hash, fake_commit3_hash]) 
    end   
    let(:expected_name_list) { [['John Doe', 'John Doe'], ['Mike Smith', 'Mike Smith']] }

    it 'returns unique name list' do
      expect(name_list(Commit.order_by_name)).to eq(expected_name_list)
    end
  end
end 