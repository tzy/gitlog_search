require 'rails_helper'
require 'helpers'

describe Commit do 
  include Helpers

  context 'scopes: ' do
      before do
        Commit.delete_all
      end 
      let(:fake_commit1) { Commit.create!(fake_commit1_hash) }
      let(:fake_commit2) { Commit.create!(fake_commit2_hash) }
      let(:fake_commit3) { Commit.create!(fake_commit3_hash) }

    context 'ref' do
      it 'filters records by ref' do
        expect(Commit.ref('84a5')).to eq([fake_commit1])
      end

      it 'filters records by author' do
        expect(Commit.author('Mike Smith')).to eq([fake_commit1, fake_commit3])
      end
      
      it 'filters records by date' do
        expect(Commit.date('2012-03-20')).to eq([fake_commit1, fake_commit2])
      end

      it 'returns commits order by name' do
        expect(Commit.order_by_name).to eq([fake_commit2, fake_commit1, fake_commit3])
      end
    end 
  end
end 