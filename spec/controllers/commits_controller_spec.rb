require 'rails_helper'
require 'helpers'

describe CommitsController do 
  include Helpers

  context '#index' do 
    before do
      Commit.delete_all
      @fake_commit1 = Commit.create!(fake_commit1_hash) 
      @fake_commit2 = Commit.create!(fake_commit2_hash) 
      @fake_commit3 = Commit.create!(fake_commit3_hash)
    end  

    it 'show a list of all users' do
      get :index     
      expect(assigns[:commits]).to eq([@fake_commit1, @fake_commit2, @fake_commit3])
    end 

    it 'filters results by ref' do
      get :index, :ref => '84a5'    
      expect(assigns[:commits]).to eq([@fake_commit1])
    end

    it 'filters results by author' do
      get :index, :author => 'Mike Smith' 
      expect(assigns[:commits]).to eq([@fake_commit1, @fake_commit3])
    end

    it 'filters results by date' do
      get :index, :date => '2012-03-20' 
      expect(assigns[:commits]).to eq([@fake_commit1, @fake_commit2])
    end

    it 'filters results by author and date' do
      get :index, :author => 'Mike Smith', :date => '2012-03-19' 
      expect(assigns[:commits]).to eq([@fake_commit3])
    end

    it 'filters results by ref author and date' do
      get :index, :ref => '5f942fdf0892f', :author => 'John Doe', :date => '2012-03-20' 
      expect(assigns[:commits]).to eq([@fake_commit2])
    end
  end 
end 