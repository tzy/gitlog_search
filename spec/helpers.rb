module Helpers
  def fake_commit1_hash 
    {
      :ref => '84a5142739f8ee9b24a92791cf34720683dd0213', 
      :author => 'Mike Smith <mike@somedomain.com>',
      :name => 'Mike Smith', 
      :email => 'mike@somedomain.com',
      :date => 'Tue Mar 20 15:29:02 2012 -0400', 
      :comments => 'Fixing a lot of broken tests'
    }
  end

  def fake_commit2_hash
    {
      :ref => '0662da725f942fdf0892fae5a44944e538bb3fe4', 
      :author => 'John Doe <John.Doe@raritan.com>',
      :name => 'John Doe', 
      :email => 'John.Doe@raritan.com',
      :date => 'Tue Mar 20 17:16:23 2012 -0400', 
      :comments => 'rollup interval converter testing'
    }
  end

  def fake_commit3_hash
    {
      :ref => '44aa90243bc07fcc6fea31799072c6b7be643075', 
      :merge => 'ae5a30e 76bde21',
      :author => 'Mike Smith <mike@somedomain.com>',
      :name => 'Mike Smith', 
      :email => 'mike@somedomain.com',
      :date => 'Mon Mar 19 11:10:14 2012 -0400', 
      :comments => "Merge branch 'master' into rollup_table_split_up"
    }
  end
end